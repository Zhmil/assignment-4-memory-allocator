#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <stddef.h>
#include <stdint.h>

#define TEST_HEAP_SIZE 2048
#define BASE_ALLOC_SIZE 200

static struct block_header* get_header(void *ptr) {
    return (struct block_header *) ((uint8_t*) ptr - offsetof(struct block_header, contents));
}

void* test_init_heap() {
    void *heap = heap_init(TEST_HEAP_SIZE);
    assert(heap != NULL);
    return heap;
}

void test_memory_allocation() {
    void *allocated_block = _malloc(BASE_ALLOC_SIZE);
    assert(allocated_block != NULL);
    _free(allocated_block);
    heap_term();
}

void test_allocated_one_block_then_released() {
    void *b1 = _malloc(BASE_ALLOC_SIZE);
    void *b2 = _malloc(BASE_ALLOC_SIZE * 2);

    _free(b2);

    struct block_header *header1 = get_header(b1);
    struct block_header *header2 = get_header(b2);

    assert(header1->next == header2);
    assert(header2->is_free);

    heap_term();
}

void test_allocated_two_blocks_then_released() {
    void *b1 = _malloc(BASE_ALLOC_SIZE);
    void *b2 = _malloc(BASE_ALLOC_SIZE*2);
    void *b3 = _malloc(BASE_ALLOC_SIZE*3);
    assert(b1 != NULL);
    assert(b2 != NULL);
    assert(b3 != NULL);

    struct block_header* header1 = get_header(b1);
    struct block_header* header2 = get_header(b2);
    struct block_header* header3 = get_header(b3);

    assert(header1->next == header2);
    assert(header2->next == header3);

    _free(b1);
    _free(b2);

    assert(header1->is_free);
    assert(header2->is_free);

    heap_term();
}

void test_grow_heap_extended(void *heap) {
    size_t initial_region_size = ((struct region *) heap)->size;

    void *block = _malloc(5 * TEST_HEAP_SIZE);
    struct block_header *new_block_header = get_header(block);
    size_t expanded_region_size = ((struct region *) heap)->size;

    assert(initial_region_size < expanded_region_size);
    assert(new_block_header);
    assert(new_block_header->capacity.bytes == 5 * TEST_HEAP_SIZE);

    _free(block);
    heap_term();
}

void test_grow_heap_new_region() {
    void *hole = mmap(HEAP_START + REGION_MIN_SIZE, BASE_ALLOC_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE, -1, 0);
    assert(hole == HEAP_START + REGION_MIN_SIZE);

    void *new_block = _malloc(REGION_MIN_SIZE - offsetof(struct block_header, contents));
    struct block_header *new_block_header = get_header(new_block);

    assert(new_block != NULL);
    assert(new_block != hole);
    assert(new_block_header->capacity.bytes >= (REGION_MIN_SIZE - offsetof(struct block_header, contents)));

    munmap(hole, BASE_ALLOC_SIZE);
    _free(new_block);
    heap_term();
}

int main() {
    void *heap = test_init_heap();
    test_memory_allocation();
    test_allocated_one_block_then_released();
    test_allocated_two_blocks_then_released();
    test_grow_heap_extended(heap);
    test_grow_heap_new_region();
    return 0;
}